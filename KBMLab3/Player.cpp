
#pragma once

#include "stdafx.h"
#include "player.h"
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

Player::Player(char * name) : playerName(name), hands_won(0), hands_lost(0), player_cards() {
	ifstream file;
	file.open(name);

	//Check if the function was able to open the file. 
	if (!file.is_open()) {
		return;
	}

	char * word = 0;               //variable used to store each line of the file

	vector<char *> player_data;

	while (file >> word) {
		player_data.push_back(word);
	}

	if (player_data.size() != 3) {
		return;
	}

	this->playerName = player_data[0];
	this->hands_won = stoi(player_data[1]);
	this->hands_lost = stoi(player_data[2]);

}

ostream & operator<<(ostream & out, const Player & p) {
	cout << "Player Name: " << p.playerName << " Hands Won: " << p.hands_won << " Hands Lost: " << p.hands_lost << endl;
	return out;
}